import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'

import Mapa from '../components/mapa'

export default function Routes () {
  return (
    <Router>
      <Route path='/' exact component={Mapa} />
      <Route path='/mapa' component={Mapa} />
      {/* <Route from='*' component={Mapa} /> */}
    </Router>
  )
}
