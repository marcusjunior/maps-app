import {
  MARCADOR_ADICIONADO,
  LATITUDE_ADICIONADA,
  LOGINTUDE_ADICIONADA,
  TEXTO_ADICIONADO
} from '../constants/ActionTypes'

const INITIAL_STATE = {
  texto: '',
  latitude: -30.1087957,
  logintude: -51.3172248,
  exibir: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TEXTO_ADICIONADO:
      return {
        ...state,
        texto: action.payload
      }
    case LATITUDE_ADICIONADA:
      return {
        ...state,
        latitude: action.payload
      }
    case LOGINTUDE_ADICIONADA:
      return {
        ...state,
        logintude: action.payload
      }
    case MARCADOR_ADICIONADO:

      console.log(action)
      return {
        ...state,
        // latitude: action.payload.latitude,
        // logintude: action.payload.logintude,
        exibir: true
      }
    default:
      return state
  }
}
