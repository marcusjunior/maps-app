export const styles = {
  container: {
    padding: 5,
    display: 'flex',
    alignItems: 'baseline'
  },
  panel: {
    margin: 20,
    padding: 20
  },
  coordenadas: {
    width: '45%',
    align: 'left',
    display: 'flex',
    padding: 5
  },
  mensagem: {
    align: 'left',
    display: 'flex',
    padding: 5
  },
  marcador: {
    fontFamily: 'Roboto',
    borderRadius: 3
  },
  mapa: {
    width: '100% !important',
    height: '70% !important'
  }
}
