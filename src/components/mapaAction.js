import * as types from '../constants/ActionTypes'

export const adicionarTexto = event => ({
  type: types.TEXTO_ADICIONADO,
  payload: event.target.value
})

export const adicionarLatitude = event => ({
  type: types.LATITUDE_ADICIONADA,
  payload: Number(event.target.value)
})

export const adicionarLongitude = event => ({
  type: types.LOGINTUDE_ADICIONADA,
  payload: Number(event.target.value)
})

export const adicionarMarcador = (latitude, longitude) => ({
  type: types.MARCADOR_ADICIONADO,
  payload: {
    latitude,
    longitude
  }
})
