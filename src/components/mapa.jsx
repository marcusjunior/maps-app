import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { styles } from './mapa.jss'
import 'typeface-roboto'

import {
  adicionarMarcador,
  adicionarLatitude,
  adicionarLongitude,
  adicionarTexto
} from './mapaAction'

import {
  Paper,
  Typography,
  TextField,
  Button,
  withStyles,
  Container
} from '@material-ui/core'

import {
  GoogleApiWrapper,
  InfoWindow,
  Map,
  Marker
} from 'google-maps-react'

const Mapa = (props) => {
  const { classes } = props

  const dispatch = useDispatch()

  const latitude = useSelector((state) => state.mapa.latitude)
  const logintude = useSelector((state) => state.mapa.logintude)
  const texto = useSelector((state) => state.mapa.texto)
  const exibir = useSelector((state) => state.mapa.exibir)

  const handleLatitude = e => dispatch(adicionarLatitude(e))
  const handleLogintude = e => dispatch(adicionarLongitude(e))
  const handleTexto = e => dispatch(adicionarTexto(e))

  const [activeMarker, setActiveMarker] = React.useState()

  return (
    <div>
      <Container maxWidth='lg' data-test='container-mapa'>
        <Container maxWidth='md'>
          <Paper className={classes.panel}>
            <Container className={classes.container}>
              <TextField
                className={classes.mensagem}
                name='mensagem'
                label='Informe o texto da mensagem'
                value={texto}
                onChange={handleTexto}
                variant='outlined'
                fullWidth
                data-test='input-texto'
              />
            </Container>
            <Container className={classes.container}>
              <TextField
                className={classes.coordenadas}
                name='latitude'
                label='Latitude'
                value={latitude}
                onChange={handleLatitude}
                variant='outlined'
                type='number'
              />
              <TextField
                className={classes.coordenadas}
                name='longitude'
                label='Longitude'
                value={logintude}
                onChange={handleLogintude}
                variant='outlined'
                type='number'
              />
              <Button
                variant='outlined'
                className={classes.marcador}
                size='large'
                onClick={() => dispatch(adicionarMarcador(latitude, logintude))}
                data-test='botao-marcar'
              >
                                Marcar
              </Button>
            </Container>
          </Paper>
        </Container>
      </Container>

      <Map
        className={classes.mapa}
        google={props.google}
        zoom={14}
        initialCenter={{ lat: latitude, lng: logintude }}
        center={{ lat: latitude, lng: logintude }}
        data-test='div-mapa'
      >

        <Marker
          id='marcador'
          onClick={(props, marker, e) => setActiveMarker(marker)}
          title='clique para exibir o texto.'
          position={{ lat: latitude, lng: logintude }}
          visible={exibir}
        />

        <InfoWindow
          marker={activeMarker}
          visible
        >
          <Paper>
            <Typography
              component='p'
            >
              {texto}
            </Typography>
          </Paper>
        </InfoWindow>
      </Map>

    </div>
  )
}

Mapa.propTypes = {
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    panel: PropTypes.string.isRequired,
    coordenadas: PropTypes.string.isRequired,
    mensagem: PropTypes.string.isRequired,
    marcador: PropTypes.string.isRequired,
    mapa: PropTypes.string.isRequired
  }).isRequired,
  latitude: PropTypes.number.isRequired,
  logintude: PropTypes.number.isRequired,
  texto: PropTypes.string.isRequired,
  exibir: PropTypes.bool.isRequired,
  activeMarker: PropTypes.object.isRequired
}

export default GoogleApiWrapper({
  api: (process.env.GOOGLE_API_KEY_GOES_HERE)
})(withStyles(styles)(Mapa))
