import React from 'react'
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { mount, render } from 'enzyme'
import Mapa from './mapa'

describe('Testes unitários componente de Mapa', () => {
  const mockStore = configureStore()

  const initialState = {
    mapa: {
      texto: '',
      latitude: -30.1087957,
      logintude: -51.3172248,
      exibir: false
    }
  }

  let store
  let wrapper

  it('Renderizar componente de mapa', () => {
    store = mockStore(initialState)

    wrapper = mount(
      <Provider store={store}>
        <Mapa />
      </Provider>
    )

    expect(wrapper).toMatchSnapshot()
  })
})
