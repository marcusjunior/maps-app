import { combineReducers } from 'redux'
import mapaReducer from '../components/mapaReducer'

const rootReducer = combineReducers({
  mapa: mapaReducer
})

export default rootReducer
