# Geral

Aplicação REACT integrada com google maps

## Tecnologias utilizadas

* ReactJS
* Material UI
* Redux
* Enzyme
* JavaScript Standard Style
* Google Maps
* Cypress

## Instalação local

Após ter sido realizado o clone do projeto, executar o comando:

``` npm install ```

## Utilização

Para iniciar a aplicação, executar o comando: 

``` npm start ```

Após a aplicação ter iniciado, a mesma estará disponível na url:

http://localhost:3000

Para adicionar um marcador, basta informa o texto e os parametros de latitude e longitude e clicar no botão "marcar". 

Para visualizar o texto no mapa deve-se clicar no ícone do marcador.

Para executar os testes da aplicação, executar o comando:

``` npm test ```

Para executar os testes e2e, executar o comando:

``` npm run test:cypress ```

## Autor

Marcus Júnior