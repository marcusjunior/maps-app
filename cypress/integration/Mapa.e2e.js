describe('Mapa E2E', async () => {
  beforeEach(() => {
    window.localStorage.setItem('ACTIVE_TOGGLES', 'NENHUM')
  })

  it('Deveria mostrar a tela de mapa', () => {
    cy.server()
    cy.visit('mapa')

    cy.get('[data-test=container-mapa]').should('be.visible')
  })

  it('Deveria adicionar um marcador no mapa', () => {
    cy.server()
    cy.visit('mapa')

    cy.get('[data-test=input-texto]').type('Texto para exibir no marcador')
    cy.get('[data-test="botao-marcar"]').click()

    cy.get('[data-test=container-mapa]').should('be.visible')
  })
})
